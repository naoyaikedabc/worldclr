# WorldCLR - CLR binding for WORLD

Morise's World Vocoder is a fast and high-quality vocoder.
Code is written in C++/CLI.

For more information, please visit Morise's World repository (https://github.com/mmorise/World) and the official website of World Vocoder (http://ml.cs.yamanashi.ac.jp/world/)

## APIs

```open Brainchild.Signal.WorldCLR
let world = new World()
let option = new DioOption()
let frame_period = option.frame_period
let sourceObject = world.Dio(stream, fs, option)
let filterObject = world.CheapTrick(stream, sourceObject)
world.D4C(stream, sourceObject)
let signal_length = stream.Length
let reconst = world.Synthesis(sourceObject, filterObject, frame_period, signal_length)
```

## TODO
Realtime Synthesizer