#pragma once
namespace Brainchild {
	namespace Signal {
		namespace WorldCLR {
			public ref class FilterObject
			{
			public:
				array<double, 2>^ SpectrumEnvelope;
			};
		}
	}
}