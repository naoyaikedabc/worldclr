#pragma once

#include <world/dio.h>

namespace Brainchild {
	namespace Signal {
		namespace WorldCLR {
			public ref class DioOption
			{
			public:
				double f0_floor;
				double f0_ceil;
				double channels_in_octave;
				double frame_period;  // msec
				int speed;  // (1, 2, ..., 12)
				double allowed_range;  // Threshold used for fixing the F0 contour.

				DioOption() {
					::DioOption dopt = { 0 };
					InitializeDioOption(&dopt);
					this->f0_floor = dopt.f0_floor;
					this->f0_ceil = dopt.f0_ceil;
					this->channels_in_octave = dopt.channels_in_octave;
					this->allowed_range = dopt.allowed_range;
					this->frame_period = dopt.frame_period;
					this->speed = dopt.speed;
				}
			};
		}
	}
}

