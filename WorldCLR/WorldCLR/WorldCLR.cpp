// これは メイン DLL ファイルです。

#include "stdafx.h"

#include "WorldCLR.h"

// これは メイン DLL ファイルです。

#include "stdafx.h"

#include "WorldCLR.h"
#include "DioOption.h"
#include "SourceObject.h"
#include "FilterObject.h"
#include "world\dio.h"
#include "world\common.h"
#include "world\synthesis.h"
#include "world\d4c.h"
#include "world\cheaptrick.h"

Brainchild::Signal::WorldCLR::SourceObject^ Brainchild::Signal::WorldCLR::World::Dio(array<double>^ signal, int fs, Brainchild::Signal::WorldCLR::DioOption^ option) {
	Brainchild::Signal::WorldCLR::SourceObject^ sourceObject = gcnew Brainchild::Signal::WorldCLR::SourceObject();
	::DioOption dopt;
	::InitializeDioOption(&dopt);
	dopt.allowed_range = option->allowed_range;
	dopt.channels_in_octave = option->channels_in_octave;
	dopt.f0_ceil = option->f0_ceil;
	dopt.f0_floor = option->f0_floor;
	dopt.frame_period = option->frame_period;
	dopt.speed = option->speed;

	int sX = signal->Length;
	int f0_length = ::GetSamplesForDIO(fs, sX, option->frame_period);
	double* temporalPotions = new double[f0_length];
	double* f0 = new double[f0_length];
	pin_ptr<double> X = &signal[0];

	::Dio(X, sX, fs, &dopt, temporalPotions, f0);

	sourceObject->F0 = gcnew array<double>(f0_length);
	sourceObject->TemporalPositions = gcnew array<double>(f0_length);
	sourceObject->FS = fs;

	for (int i = 0; i < f0_length; i++) {
		sourceObject->F0[i] = f0[i];
		sourceObject->TemporalPositions[i] = temporalPotions[i];
	}
	delete f0;
	delete temporalPotions;

	return sourceObject;
}

void Brainchild::Signal::WorldCLR::World::D4C(array<double>^ signal, Brainchild::Signal::WorldCLR::SourceObject^ sourceObject) {
	::D4COption dopt = { 0 };
	::InitializeD4COption(&dopt);

	int sX = signal->Length;
	pin_ptr<double> X = &signal[0];
	pin_ptr<double> temporalPositions = &sourceObject->TemporalPositions[0];
	pin_ptr<double> f0 = &sourceObject->F0[0];
	array<double, 2>^ ap = gcnew array<double, 2>(sourceObject->F0->Length, sourceObject->FFT_SIZE / 2 + 1);

	double** aperiodicity = new double *[sourceObject->F0->Length];
	for (int i = 0; i < sourceObject->F0->Length; ++i) {
		aperiodicity[i] =
			new double[sourceObject->FFT_SIZE / 2 + 1];
	}

	::D4C(X, sX, sourceObject->FS, temporalPositions, f0, sourceObject->F0->Length, sourceObject->FFT_SIZE, &dopt, aperiodicity);

	for (int i = 0; i < sourceObject->F0->Length; i++) {
		for (int j = 0; j < sourceObject->FFT_SIZE / 2 + 1; j++) {
			ap[i, j] = aperiodicity[i][j];
		}
	}
	for (int i = 0; i < sourceObject->F0->Length; i++) {
		delete aperiodicity[i];
	}
	delete aperiodicity;

	sourceObject->Aperiodicity = ap;
}

void Brainchild::Signal::WorldCLR::World::D4C(array<double>^ signal, Brainchild::Signal::WorldCLR::SourceObject^ sourceObject, int samplingRate, int fft_size) {
	::D4COption dopt = { 0 };
	::InitializeD4COption(&dopt);

	int sX = signal->Length;
	pin_ptr<double> X = &signal[0];
	pin_ptr<double> temporalPositions = &sourceObject->TemporalPositions[0];
	pin_ptr<double> f0 = &sourceObject->F0[0];
	array<double, 2>^ ap = gcnew array<double, 2>(sourceObject->F0->Length, fft_size / 2 + 1);

	double** aperiodicity = new double *[sourceObject->F0->Length];
	for (int i = 0; i < sourceObject->F0->Length; ++i) {
		aperiodicity[i] =
			new double[fft_size / 2 + 1];
	}

	::D4C(X, sX, samplingRate, temporalPositions, f0, sourceObject->F0->Length, fft_size, &dopt, aperiodicity);

	for (int i = 0; i < sourceObject->F0->Length; i++) {
		for (int j = 0; j < fft_size / 2 + 1; j++) {
			ap[i, j] = aperiodicity[i][j];
		}
	}
	for (int i = 0; i < sourceObject->F0->Length; i++) {
		delete aperiodicity[i];
	}
	delete aperiodicity;

	sourceObject->Aperiodicity = ap;
}

void Brainchild::Signal::WorldCLR::World::D4C(array<double>^ signal, Brainchild::Signal::WorldCLR::SourceObject^ sourceObject, int fft_size) {
	::D4COption dopt = { 0 };
	::InitializeD4COption(&dopt);

	int sX = signal->Length;
	pin_ptr<double> X = &signal[0];
	pin_ptr<double> temporalPositions = &sourceObject->TemporalPositions[0];
	pin_ptr<double> f0 = &sourceObject->F0[0];
	array<double, 2>^ ap = gcnew array<double, 2>(sourceObject->F0->Length, fft_size / 2 + 1);

	double** aperiodicity = new double *[sourceObject->F0->Length];
	for (int i = 0; i < sourceObject->F0->Length; ++i) {
		aperiodicity[i] =
			new double[fft_size / 2 + 1];
	}

	::D4C(X, sX, sourceObject->FS, temporalPositions, f0, sourceObject->F0->Length, fft_size, &dopt, aperiodicity);

	for (int i = 0; i < sourceObject->F0->Length; i++) {
		for (int j = 0; j < fft_size / 2 + 1; j++) {
			ap[i, j] = aperiodicity[i][j];
		}
	}
	for (int i = 0; i < sourceObject->F0->Length; i++) {
		delete aperiodicity[i];
	}
	delete aperiodicity;

	sourceObject->Aperiodicity = ap;
}

Brainchild::Signal::WorldCLR::FilterObject^ Brainchild::Signal::WorldCLR::World::CheapTrick(array<double>^ signal, Brainchild::Signal::WorldCLR::SourceObject^ sourceObject) {
	Brainchild::Signal::WorldCLR::FilterObject^ filterObject = gcnew Brainchild::Signal::WorldCLR::FilterObject();
	::CheapTrickOption copt = { 0 };
	::InitializeCheapTrickOption(sourceObject->FS, &copt);

	int sX = signal->Length;
	pin_ptr<double> X = &signal[0];
	pin_ptr<double> temporalPositions = &sourceObject->TemporalPositions[0];
	pin_ptr<double> f0 = &sourceObject->F0[0];
	int fft_size =
		GetFFTSizeForCheapTrick(sourceObject->FS, &copt);
	sourceObject->FFT_SIZE = fft_size;

	array<double, 2>^ sp = gcnew array<double, 2>(sourceObject->F0->Length, fft_size / 2 + 1);
	double** spec = new double *[sourceObject->F0->Length];
	for (int i = 0; i < sourceObject->F0->Length; ++i) {
		spec[i] =
			new double[fft_size / 2 + 1];
	}

	::CheapTrick(X, sX, sourceObject->FS, temporalPositions, f0, sourceObject->F0->Length, &copt, spec);

	for (int i = 0; i < sourceObject->F0->Length; i++) {
		for (int j = 0; j < fft_size / 2 + 1; j++) {
			sp[i, j] = spec[i][j];
		}
	}

	for (int i = 0; i < sourceObject->F0->Length; i++) {
		delete spec[i];
	}
	delete spec;

	filterObject->SpectrumEnvelope = sp;

	return filterObject;
}

Brainchild::Signal::WorldCLR::FilterObject^ Brainchild::Signal::WorldCLR::World::CheapTrick(array<double>^ signal, Brainchild::Signal::WorldCLR::SourceObject^ sourceObject, Brainchild::Signal::WorldCLR::CheapTrickOption^ cto) {
	Brainchild::Signal::WorldCLR::FilterObject^ filterObject = gcnew Brainchild::Signal::WorldCLR::FilterObject();
	::CheapTrickOption copt = { 0 };

	copt.f0_floor = cto->f0_floor;
	copt.fft_size = cto->fft_size;
	copt.q1 = cto->q1;

	int sX = signal->Length;
	pin_ptr<double> X = &signal[0];
	pin_ptr<double> temporalPositions = &sourceObject->TemporalPositions[0];
	pin_ptr<double> f0 = &sourceObject->F0[0];

	int fft_size = copt.fft_size;

	array<double, 2>^ sp = gcnew array<double, 2>(sourceObject->F0->Length, fft_size / 2 + 1);
	double** spec = new double *[sourceObject->F0->Length];
	for (int i = 0; i < sourceObject->F0->Length; ++i) {
		spec[i] =
			new double[fft_size / 2 + 1];
	}

	::CheapTrick(X, sX, sourceObject->FS, temporalPositions, f0, sourceObject->F0->Length, &copt, spec);

	for (int i = 0; i < sourceObject->F0->Length; i++) {
		for (int j = 0; j < fft_size / 2 + 1; j++) {
			sp[i, j] = spec[i][j];
		}
	}

	for (int i = 0; i < sourceObject->F0->Length; i++) {
		delete spec[i];
	}
	delete spec;

	filterObject->SpectrumEnvelope = sp;

	return filterObject;
}

array<double>^ Brainchild::Signal::WorldCLR::World::Synthesis(Brainchild::Signal::WorldCLR::SourceObject^ sourceObject, Brainchild::Signal::WorldCLR::FilterObject^ filterObject, double frame_period, int signalLength) {
	array<double>^ result = nullptr;

	double* buffer = new double[signalLength];
	pin_ptr<double> f0 = &sourceObject->F0[0];
	int f0length = sourceObject->F0->Length;

	double** aperiodicity = new double *[sourceObject->F0->Length];
	for (int i = 0; i < sourceObject->F0->Length; ++i) {
		aperiodicity[i] =
			new double[sourceObject->FFT_SIZE / 2 + 1];
	}

	double** spec = new double *[sourceObject->F0->Length];
	for (int i = 0; i < sourceObject->F0->Length; ++i) {
		spec[i] =
			new double[sourceObject->FFT_SIZE / 2 + 1];
	}

	for (int i = 0; i < sourceObject->F0->Length; i++) {
		for (int j = 0; j < sourceObject->FFT_SIZE / 2 + 1; j++) {
			aperiodicity[i][j] = sourceObject->Aperiodicity[i, j];
		}
	}

	for (int i = 0; i < sourceObject->F0->Length; i++) {
		for (int j = 0; j < sourceObject->FFT_SIZE / 2 + 1; j++) {
			spec[i][j] = filterObject->SpectrumEnvelope[i, j];
		}
	}

	::Synthesis(f0, f0length, spec, aperiodicity, sourceObject->FFT_SIZE, frame_period, sourceObject->FS, signalLength, buffer);

	result = gcnew array<double>(signalLength);

	for (int i = 0; i < signalLength; i++) {
		result[i] = buffer[i];
	}

	delete buffer;
	delete aperiodicity;
	delete spec;

	return result;
}

array<double>^ Brainchild::Signal::WorldCLR::World::Synthesis(Brainchild::Signal::WorldCLR::SourceObject^ sourceObject, Brainchild::Signal::WorldCLR::FilterObject^ filterObject, double frame_period, int signalLength, int fft_size) {
	array<double>^ result = nullptr;

	double* buffer = new double[signalLength];
	pin_ptr<double> f0 = &sourceObject->F0[0];
	int f0length = sourceObject->F0->Length;

	double** aperiodicity = new double *[sourceObject->F0->Length];
	for (int i = 0; i < sourceObject->F0->Length; ++i) {
		aperiodicity[i] =
			new double[fft_size / 2 + 1];
	}

	double** spec = new double *[sourceObject->F0->Length];
	for (int i = 0; i < sourceObject->F0->Length; ++i) {
		spec[i] =
			new double[fft_size / 2 + 1];
	}

	for (int i = 0; i < sourceObject->F0->Length; i++) {
		for (int j = 0; j < fft_size / 2 + 1; j++) {
			aperiodicity[i][j] = sourceObject->Aperiodicity[i, j];
		}
	}

	for (int i = 0; i < sourceObject->F0->Length; i++) {
		for (int j = 0; j < fft_size / 2 + 1; j++) {
			spec[i][j] = filterObject->SpectrumEnvelope[i, j];
		}
	}

	::Synthesis(f0, f0length, spec, aperiodicity, fft_size, frame_period, sourceObject->FS, signalLength, buffer);

	result = gcnew array<double>(signalLength);

	for (int i = 0; i < signalLength; i++) {
		result[i] = buffer[i];
	}

	delete buffer;
	delete aperiodicity;
	delete spec;

	return result;
}

array<double>^ Brainchild::Signal::WorldCLR::World::Synthesis(Brainchild::Signal::WorldCLR::SourceObject^ sourceObject, Brainchild::Signal::WorldCLR::FilterObject^ filterObject, double frame_period, int signalLength, int samplingRate, int fft_size) {
	array<double>^ result = nullptr;

	double* buffer = new double[signalLength];
	pin_ptr<double> f0 = &sourceObject->F0[0];
	int f0length = sourceObject->F0->Length;

	double** aperiodicity = new double *[sourceObject->F0->Length];
	for (int i = 0; i < sourceObject->F0->Length; ++i) {
		aperiodicity[i] =
			new double[fft_size / 2 + 1];
	}

	double** spec = new double *[sourceObject->F0->Length];
	for (int i = 0; i < sourceObject->F0->Length; ++i) {
		spec[i] =
			new double[fft_size / 2 + 1];
	}

	for (int i = 0; i < sourceObject->F0->Length; i++) {
		for (int j = 0; j < fft_size / 2 + 1; j++) {
			aperiodicity[i][j] = sourceObject->Aperiodicity[i, j];
		}
	}

	for (int i = 0; i < sourceObject->F0->Length; i++) {
		for (int j = 0; j < fft_size / 2 + 1; j++) {
			spec[i][j] = filterObject->SpectrumEnvelope[i, j];
		}
	}

	::Synthesis(f0, f0length, spec, aperiodicity, fft_size, frame_period, samplingRate, signalLength, buffer);

	result = gcnew array<double>(signalLength);

	for (int i = 0; i < signalLength; i++) {
		result[i] = buffer[i];
	}

	delete buffer;
	delete aperiodicity;
	delete spec;

	return result;
}