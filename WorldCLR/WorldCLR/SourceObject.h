#pragma once
namespace Brainchild {
	namespace Signal {
		namespace WorldCLR {
			public ref class SourceObject
			{
			public:
				array<double>^ F0;
				array<double>^ TemporalPositions;
				array<double, 2>^ Aperiodicity;
				int FS;
				int FFT_SIZE;
				SourceObject() {
					F0 = nullptr;
					TemporalPositions = nullptr;
				}
			};
		}
	}
}