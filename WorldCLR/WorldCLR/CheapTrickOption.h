#pragma once

#include <world/cheaptrick.h>
#include <world/common.h>

namespace Brainchild {
	namespace Signal {
		namespace WorldCLR {
			public ref class CheapTrickOption
			{
			public:
				double q1;
				double f0_floor;
				int fft_size;

				CheapTrickOption(int fs) {
					::CheapTrickOption copt = { 0 };
					InitializeCheapTrickOption(fs, &copt);

					this->f0_floor = copt.f0_floor;
					this->fft_size = fft_size;
					this->q1 = copt.q1;
				}
			};
		}
	}
}

