// WorldCLR.h

#pragma once
#include "SourceObject.h"
#include "FilterObject.h"
#include "DioOption.h"
#include "CheapTrickOption.h"

using namespace System;

namespace Brainchild {
	namespace Signal {
		namespace WorldCLR {
			public ref class World {
			public:
				Brainchild::Signal::WorldCLR::SourceObject ^ Dio(array<double>^ signal, int fs, Brainchild::Signal::WorldCLR::DioOption ^ option);

				void D4C(array<double>^ signal, Brainchild::Signal::WorldCLR::SourceObject^ sourceObject);

				void D4C(array<double>^ signal, Brainchild::Signal::WorldCLR::SourceObject^ sourceObject, int samplingRate, int fft_size);

				void D4C(array<double>^ signal, Brainchild::Signal::WorldCLR::SourceObject^ sourceObject, int fft_size);

				Brainchild::Signal::WorldCLR::FilterObject^ CheapTrick(array<double>^ signal, Brainchild::Signal::WorldCLR::SourceObject^ sourceObject);

				Brainchild::Signal::WorldCLR::FilterObject^ CheapTrick(array<double>^ signal, Brainchild::Signal::WorldCLR::SourceObject^ sourceObject, Brainchild::Signal::WorldCLR::CheapTrickOption^ cto);

				array<double>^ Brainchild::Signal::WorldCLR::World::Synthesis(Brainchild::Signal::WorldCLR::SourceObject^ sourceObject, Brainchild::Signal::WorldCLR::FilterObject^ filterObject, double frame_period, int signalLength);

				array<double>^ Brainchild::Signal::WorldCLR::World::Synthesis(Brainchild::Signal::WorldCLR::SourceObject^ sourceObject, Brainchild::Signal::WorldCLR::FilterObject^ filterObject, double frame_period, int signalLength, int fft_size);

				array<double>^ Brainchild::Signal::WorldCLR::World::Synthesis(Brainchild::Signal::WorldCLR::SourceObject^ sourceObject, Brainchild::Signal::WorldCLR::FilterObject^ filterObject, double frame_period, int signalLength, int samplingRate, int fft_size);
			};
		}
	}
}